// From:
// https://arjunsreedharan.org/post/148675821737/memory-allocators-101-write-a-simple-memory

#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <stdio.h>

void simple_writer(const char *string)
{
    size_t size = 0;
    while (string[size])
        size++;
    write(STDOUT_FILENO, string, size * sizeof(char));
}



struct header_t
{
    size_t size;
    unsigned is_free;
    struct header_t *next;
};


pthread_mutex_t global_malloc_lock;
struct header_t *first_header = NULL, *tail = NULL;


struct header_t *get_free_block(size_t size)
{
    struct header_t *curr = first_header;
    while (curr) {
        if (curr->is_free && curr->size >= size)
            return curr;
        curr = curr->next;
    }
    return NULL; // No large enough free block
}


void free(void *block)
{
    struct header_t *header, *tmp;
    void *programbreak;


    if (!block) {
        return;
    }

    pthread_mutex_lock(&global_malloc_lock);
    header = (struct header_t *) block - 1;
    programbreak = sbrk(0);

    if ((char *)block + header->size == programbreak) {
        if (first_header == tail) {
            first_header = tail = NULL;
        } else {
            tmp = first_header;
            while (tmp) {
                if (tmp->next == tail) {
                    tmp->next = NULL;
                    tail = tmp;
                }
                tmp = tmp->next;
            }
        }
        sbrk(0 - header->size - sizeof(struct header_t));
        pthread_mutex_unlock(&global_malloc_lock);
        return;
    }
    header->is_free = 1;
    pthread_mutex_unlock(&global_malloc_lock);
}

void *malloc(size_t size)
{
    size_t total_size;
    void *block;
    struct header_t *header;

    if (!size)
        return NULL;

    pthread_mutex_lock(&global_malloc_lock);

    // First see if a current block can suffice
    header = get_free_block(size);
    if (header) {
        header->is_free = 0;
        pthread_mutex_unlock(&global_malloc_lock);
        return (void *)(header + 1);
    }

    // else make a new one.
    total_size = sizeof(struct header_t) + size;
    block = sbrk(total_size);
    if (block == (void *) -1) {
        pthread_mutex_unlock(&global_malloc_lock);
        return NULL;
    }

    // point the new header to raw memory just allocated
    header = block;
    header->size = size;
    header->is_free = 0;
    header->next = NULL;
    if (!first_header)
        first_header = header;
    if (tail)
        tail->next = header;

    tail = header;
    pthread_mutex_unlock(&global_malloc_lock);
    return (void *)(header + 1);
}


void *calloc(size_t num, size_t nsize)
{
    size_t size;
    void *block;
    if (!num || !nsize)
        return NULL;
    size = num * nsize;

    if (nsize != (size / num))
        return NULL;

    block = malloc(size);

    if (!block)
        return NULL;

    memset(block, 0, size);
    return block;
}


void *realloc(void *block, size_t size)
{
    struct header_t *header;
    void *ret;

    if (!block)
        return malloc(size);

    if (!size) {
        free(block);
        return NULL;
    }

    header = (struct header_t *)block - 1;

    if (header->size >= size)
        return block;

    ret = malloc(size);
    if (ret) {
        memcpy(ret, block, header->size);
        free(block);
    }
    return ret;
}


int main()
{
    int *ap = malloc(sizeof(int));
    free(ap);
}
