cmake_minimum_required(VERSION 3.13)
project(see)

find_package(Threads REQUIRED)

add_executable(periodic periodic.c)
target_link_libraries(periodic Threads::Threads rt)
target_compile_options(periodic PRIVATE -Werror -Wall -Wextra)
