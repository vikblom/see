#include <stdint.h>
#include <sys/signal.h>
#include <time.h>

#include <stdio.h>
#include <unistd.h>

void work_foo(union sigval sig)
{
    (void) sig;
    printf("FOO\n");
}


void work_bar(union sigval sig)
{
    (void) sig;
    printf("BAR\n");
}

/* Naive implementation that lets a timer spin up a new task for each work call.
 *
 * Probably allocates some memory we cannot get rid of.
 */
timer_t schedule_work(void (*work)(union sigval), uint32_t ms)
{
    uint64_t nanosecs = 1000 * 1000 * ms;
    struct itimerspec timespec = {
        .it_value = {
            .tv_sec = nanosecs / 1000000000,
            .tv_nsec = nanosecs % 1000000000},
        .it_interval = {
            .tv_sec = nanosecs / 1000000000,
            .tv_nsec = nanosecs % 1000000000}};

    timer_t timer_id;
    struct sigevent se = {
        .sigev_value.sival_ptr = &timer_id,
        // Each tick launched in a new thread.
        .sigev_notify = SIGEV_THREAD,
        .sigev_notify_function = work,
        .sigev_notify_attributes = NULL,
   };

    // fixme
    timer_create(CLOCK_REALTIME, &se, &timer_id);
    timer_settime(timer_id, 0, &timespec, NULL);

    return timer_id;
}


int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    timer_t foo_timer = schedule_work(work_foo, 500);
    timer_t bar_timer = schedule_work(work_bar, 750);

    sleep(5);

    timer_delete(foo_timer);
    timer_delete(bar_timer);

    printf("%s\n", argv[1]);

    return 0;
}
