#include <stdlib.h>
#include <stdio.h>
#include <fenv.h>

void foo()
{
    printf("HELLO\n");
    printf("Test");

}

int main(int argc, char *argv[])
{
    fesetround(FE_TOWARDZERO);
    if (argc == 3) {
        char *ptr;

        double a = strtod(argv[1], &ptr);
        double b = strtod(argv[2], &ptr);
        double result = a * b;

        printf("%.75f\n", result);
    }
    return 0;
}
