#include <stdio.h>
#include <math.h>
#include <fenv.h>

void test_fenv()
{
    if (fetestexcept(FE_INVALID)) {
        printf("Floating point exception!\n");
    } else {
        printf("No exception.\n");
    }
}

int main(int argc, char *argv[])
{
    double a = 0.1;
    double b = 0.2;

    test_fenv();

    double c = sqrt(-1.0);

    test_fenv();

    return 0;
}
